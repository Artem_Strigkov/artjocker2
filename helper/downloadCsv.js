const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const downloadCsv = ( res, data ) => {

    const validateDb = [
        {id: 'userName',            title: 'userName',             },
        {id: 'firstName',           title: 'firstName',            },
        {id: 'lastName',            title: 'lastName',             },
        {id: 'age',                 title: 'age',                  }
    ];
    const csvWriter = createCsvWriter({
        path: 'out.csv',
        header: validateDb
    });

    csvWriter
        .writeRecords(data)
        .then(() => {
            res.status(200).download('out.csv');
        });
};

module.exports = { downloadCsv }
