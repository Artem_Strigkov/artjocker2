const express = require("express");
require("dotenv").config();
const db = require("./models");
const bodyParser = require("body-parser");

//Create app
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Create swagger
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//Public dir
app.use("/public", express.static(process.env.PUBLIC_DIR));

// Enable CORS
app.use(function(req, res, next) {
    // Websites allowed to connect
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    // Request methods to be allowed
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );
    // Request headers to be allowed
    res.setHeader("Access-Control-Allow-Headers", "X-Access-Token,Content-Type");
    // Pass to next layer of middleware
    res.setHeader("Content-Type", "application/json");
    next();
});

db.sequelize
    .sync(
        // {force:true},
        {logging: false}
    );

app.use("/api/v1", require("./routes/v1"));

app.listen(process.env.PORT, function() {
    console.log(`Node server listening on port ${process.env.PORT}`);
});
