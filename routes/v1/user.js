const express = require('express');
const userController = require('./../../controllers/user');
var router = express.Router();

//=============================== Add multer ================================
const multer = require("multer");
let upload  = multer({ storage: multer.memoryStorage() });
//================================= ROUTES ==================================
router.post('/upload', upload.single('userFile'), userController.upload);
router.get('/listing', upload.single('userFile'), userController.listing);
router.get('/download', upload.single('userFile'), userController.download);

module.exports = router;
