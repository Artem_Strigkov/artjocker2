const db = require("../models");
const csv = require('csv');

const { downloadCsv } = require("../helper/downloadCsv");

module.exports = {


    //=========================== UPLOAD CSV FILE  ==========================
    upload(req, res, next) {
        csv.parse(req.file.buffer, {columns: true}, (err, data) => {

            for (let user of data) {

                db.users.findOrCreate({where: user})
                    .then(result => {
                        return result;
                    })
                    .catch(err => {
                        return console.log(err);
                    });
            }

        });

        return res.status(200).json("CSV FILE UPLOAD");
    },


    //=========================== LISTING USERS  ===============================
    listing(req, res, next) {
        db.users.findAll()
            .then(result => {
                return res.status(200).json(result);
            })
            .catch(err => {
                return res.status(400).send(err);
            });

    },


    //====================== DOWNLOAD USERS INFORMATION ========================
    async download(req, res, next) {
        const data = await db.users.findAll();
        downloadCsv(res, data);
    }
};
