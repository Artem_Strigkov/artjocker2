module.exports = ( sequelize, Type ) => {
    return sequelize.define("users",{
        id : {
            type : Type.UUID,
            primaryKey : true,
            unique : true,
            defaultValue : Type.UUIDV4,
        },
        userName : {
            type : Type.STRING
        },
        firstName : {
            type : Type.STRING,
            validate : {
                len : [ 2, 50 ],
            }
        },
        lastName : {
            type : Type.STRING,
            validate : {
                len : [ 2, 50 ],
            }
        },
        age: {
            type: Type.INTEGER
        }
    });
};
